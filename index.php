<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <script src="jquery.min.js"></script>
    <script src="slider/jquery.bxslider.min.js"></script>
    <script src="slider.js"></script>
    <link href="slider/jquery.bxslider.css" rel="stylesheet" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  <script src="jquery-ui.js"></script>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  $(function() {
    $( "#datepicker2" ).datepicker();
  });
  </script>
</head>
<body>
<?php
    #variables 
    $email = "office@kristakis.ro";
    $nrTel = "0740 555 555";
    $Maramures = "Descopera Maramuresul";
    $slideTextOne = "Oferta Early Booking";
    $slideTextTwo = "Revelion 2016 Paris";
    $slideTextThree = "1200";
?>
    <div id="wrapper" >
        <div id="top-bar">
            <ul id="tp">
                <li class="stickTogether">
                    <p class="Contact">Gaseste-ne si pe: </p>
                </li>
                <li class="stickTogether">
                    <a href="http://www.google.ro/" ><img src="images/kristakistravel_g+.png"></a>
                </li>
                <li class="stickTogether">
                    <a href="http://www.google.ro/" ><img src="images/kristakistravel_fb.png"></a>
                </li>
                <li class="stickTogether">
                    <a href="http://www.google.ro/" ><img src="images/twitter.png"></a>
                </li>
			</ul>
               
			<ul id="contactsTopRight">
                <li>
                    <p class="Contact">Contact rapid: </p>
                </li>
                <li class="conImg">
                    <img src="images/mobil.png">
                </li>
                <li>
                    <a class="Contact"><span>0740 XXX XXX</span></a>
                </li>
                <li class="conImg">
                    <img src="images/mail.png">
                </li>
                <li>
                    <p class="Contact"><?php echo $email ?></p>
                </li>
            </ul>    
        </div>
        <div id="navbar">
            <ul id="left">
                <li>
                    <img id="logoImg" src="images/kristakistravel_Logo.png">
                </li>
			</ul>
			<ul id="right">
                <li>
                    <span style="padding-right: 120px;"
                </li>
                <li>
                    <a href="http://www.google.com">Despre noi</a>
                </li>
                <li>
                    <span class="spaces">
                    </span>
                </li>
                <li>
                    <a href="http://www.google.com">Informatii utile</a>
                </li>
                <li>
                    <span class="spaces">
                    </span>
                </li>
                <li>
                    <a href="http://www.google.com">Contact</a>
                </li>
                <li>
                    <span class="spaces">
                    </span>
                </li>
                <li>
                    <div id="searchBar" style="display:inline-block">
                        <input type="text" placeholder="Cauta tara,oras,oferta">
                        <input type="image" src="images/kristakistravel_searchButton.png" alt="Submit">
                    </div>
                </li>
            </ul>
        </div>
        <div id="smallNavBar">
          <nav class="menu">  
            <ul>
                <li>
                    <a href="http://www.google.ro"><img id="homeButton" src="images/kristakistravel_homeButton.png"></a>
                </li>
                <li>
                    <a href="http://www.google.ro">Grecia</a>
                </li>
                <li>
                    <a href="http://www.google.ro"><div id="imageContainer"><div id="maramures"><?php echo $Maramures ?></div></div></a>
                </li>
                <li>
                    <a href="http://www.google.ro">Sejururi Interne &#10148</a>
                    
                    <ul class="subMenu">
                        <li><a href="#">item1</a></li>
                        <li><a href="#">item2</a></li>
                        <li><a href="#">item3</a></li>
                        <li><a href="#">item4</a></li>
                    </ul>
                </li>
                <li>
                    <a href="http://www.google.ro">Sejururi Externe &#10148;</a>

                    <ul class="subMenu">
                        <li><a href="#">item1</a></li>
                        <li><a href="#">item2</a></li>
                        <li><a href="#">item3</a></li>
                        <li><a href="#">item4</a></li>
                    </ul>
                </li>
                <li>
                    <a href="http://www.google.ro">Bilete de Avion</a>
                </li>
            </ul>
              </nav>
        </div>
        <div id="slideshow">
            <ul class="bxslider">
                <li>
                    <div class="sliderIMG">
                    <img src="images/IMGmeniu.jpg">
                        <div class="text1">
                        <h2><?php echo $slideTextOne ?></h2>
                        </div>
                        <div class="text2">
                            <h2><?php echo $slideTextTwo ?></h2>
                        </div>
                        <div class="text3">
                                <h2>de la <span style="font-weight:bolder; font-size:16px;"><?php echo $slideTextThree ?></span> euro/pachet</h2>
                            </div>
                        <a href="#"><img src="images/kristakistravel_homepage_38.png"></a>
                    </div>
                </li>
                <li><img src="images/img2.jpg" /></li>
                <li><img src="images/img3.jpg" /></li>
                <li><img src="images/img4.jpg" /></li>
            </ul>
        </div>
        <div id="meniuCautaOferta">
            <img src="images/kristakistravel_topMeniuMotorCautar.png">
            <p>Cauta cele mai bune oferte cu ajutorul motorului</p>
            <ul class="imgList">
                <li><label>
    <input type="radio" name="travel" value="small" class="howTravel"/>
    <img src="images/kristakistravel_homepage_28.png">
		</label></li>
                <li><label>
    <input type="radio" name="travel" value="small" class="howTravel"/>
    <img src="images/kristakistravel_homepage_30.png">
		</label></li>
                <li><label>
    <input type="radio" name="travel" value="small" class="howTravel" />
    <img src="images/kristakistravel_homepage_32.png">
		</label></li>
            </ul>
            
            <input type="text" placeholder="Alege orasul de plecare">
            <input type="text" placeholder="Alege destinatia">
            <input class="check" id="datepicker" type="text" placeholder="Check-in">
            <input class="check" id="datepicker2" type="text" placeholder="Check-out">
            <input  id="butonCauta" type="image" src="images/kristakistravel_buttonMeniuCautare.png" alt="Submit">
        </div>
    <div id="oferte">
        <ul>
            <li>
                <img src="images/kristakistravel_PreturiBune.png">
                <h3>Preturi Bune!</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et turpis sed metus fermentum pellentesque.</p>
            </li>
            <li>
                <img src="images/kristakistravel_Siguranta.png">
                <h3>Agentie de Incredere</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et turpis sed metus fermentum pellentesque. </p>
            </li>
            <li>
                <img src="images/kristakistravel_plataCard.png">
                <h3>Plata online cu cardul</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et turpis sed metus fermentum pellentesque.</p>
            </li>
        </ul>
        <div class="row_one">
            <img src="images/kristakistravel_oferteCraciun.png">
            <img src="images/kristakistravel_EarlyBooking.png">
            <div id="craciun">
            <p>Oferte Craciun 2015</p>
                </div>
            <div id="reduceri">
            <p>Early Booking de la 50% reducere</p>
                </div>
            <div class="list">
            <ul>
                <li>
                    <p>Dubai,EAU<span style="padding-right: 152px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Viena,Austria<span style="padding-right: 143px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Praga,Cehia<span style="padding-right: 150px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Barcelona,Spania<span style="padding-right: 119px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Bruxelles,Olanda<span style="padding-right: 120px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Roma,Italia<span style="padding-right: 150px"></span><span class="colorInside">290Euro</span></p>
                </li>
            </ul>
                </div>
            <div class="list_two">
                <ul>
                    <li>
                        <p>Dubai,EAU<span style="padding-right: 152px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                    <li>
                        <p>Viena,Austria<span style="padding-right: 143px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                    <li>
                        <p>Praga,Cehia<span style="padding-right: 150px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                    <li>
                        <p>Barcelona,Spania<span style="padding-right: 119px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                    <li>
                        <p>Bruxelles,Olanda<span style="padding-right: 120px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                    <li>
                        <p>Roma,Italia<span style="padding-right: 150px"></span><span class="colorInside">290Euro</span></p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row_two">
            <img src="images/kristakistravel_OferteExotice.jpg">
            <img src="images/kristakistravel_OferteGrecia.png">
            <div class="text_one">
                <p>Oferte exotice 2016</p>
                <p><span style="padding-left: 50px"> de la <span style="font-weight: bold">750</span> Euro</p>
            </div>  
            <div class="text_two">
                <p>Oferte Grecia 2016</p>
                <p><span style="padding-left: 50px"> de la <span style="font-weight: bold">750</span> Euro</p>
            </div>
        </div>
        <div class="row_three">
            <img src="images/kristakistravel_oferteCityBreak.png">
            <img src="images/kristakistravel_oertaMaramures.png">
            <div id="listOne_rowThree">
            <ul>
                <li>
                    <p>Dubai,EAU<span style="padding-right: 170px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Viena,Austria<span style="padding-right: 160px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Praga,Cehia<span style="padding-right: 165px"></span><span class="colorInside">290Euro</span></p>
                </li>
                <li>
                    <p>Barcelona,Spania<span style="padding-right:134px"></span><span class="colorInside">290Euro</span></p>
                </li>
            </ul>
            </div>
        <div id="listTwo_rowThree">
        <ul>
            <li>
                <p>Dubai,EAU<span style="padding-right: 175px"></span><span class="colorInside">290Euro</span></p>
            </li>
            <li>
                <p>Viena,Austria<span style="padding-right: 165px"></span><span class="colorInside">290Euro</span></p>
            </li>
            <li>
                <p>Praga,Cehia<span style="padding-right: 170px"></span><span class="colorInside">290Euro</span></p>
            </li>
            <li>
                <p>Barcelona,Spania<span style="padding-right: 139px"></span><span class="colorInside">290Euro</span></p>
            </li>
        </ul>
    </div>
        </div>
    </div>
        <div id="phoneNumbers">
            <img src="images/kristakistravel_tePutemAjuta.png">
            <div id="contactNumbers">
                <ul>
                    <li>
            <a class="Contact"><span>0740 XXX XXX</span></a>
                        </li>
                    <li>
            <a class="Contact"><span>0740 XXX XXX</span></a>
                    </li>
                    </ul>
                </div>
        </div>
        <div id="destinatiiTuristice">
            <img src="images/DESTINATII_TURISTICE.png">
            <div id="listaDestinatii">
            <ul>
                <li><a href="#">BULGARIA</a></li>
                <li><a href="#">CIPRU</a></li>
                <li><a href="#">EMIRATELE ARABE UNITE</a></li>
                <li><a href="#">GRECIA</a></li>
                <li><a href="#">FRANTA</a></li>
                <li><a href="#">ITALIA</a></li>
                <li><a href="#">SPANIA</a></li>
                <li id="noBorder"><a href="#">TURCIA</a></li>
                
            </ul>
                </div>
        </div>
        <div id="abonareNewsletter">
            <img src="images/kristakistravel_Newsletter.png">
            <div id="newsletterStuff">
                <ul>
            <li><input type="text" placeholder="Numele Dumneavoastra" class="inp"></li>
            <li><input type="text" placeholder="Adresa de email" class="inp"></li>
            <li><input type="submit" value="Ma abonez acum!" id="submitButton"></li>
                    </ul>
                </div>
        </div>
        <div id="footer">
            <img src="images/kristakistravel_botFooter.png">
            <div id="footerDetails">
                <ul>
                    <ul>
                        <li><p class="title firstTwo">Navigare</p></li>
                        <li><a href="#">Informatii Utile</a></li>
                        <li><a href="#">Despre Noi</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                    <ul>
                        <li><p class="title firstTwo">Termeni si Conditii</p></li>
                        <li><a href="#">Termeni de utilizare</a></li>
                        <li><a href="#">ANPC</a></li>
                        <li><a href="#">Contract cu turistul</a></li>
                    </ul>
                    <ul id="issue1">
                        <li><p class="title Third">Contact</p></li>
                        <li> <a class="Contact color"><span>0740 XXX XXX</span></a></li>
                        <li> <a class="Contact color"><span>0740 XXX XXX</span></a></li>
                        <li><p>Email: ofice@kristakis.ro</p></li>
                    </ul>
                    <ul id="issue2">
                        <li><p class="title">Mesajul Tau</p></li>
                        <li class="hotfix1"><input type="text" placeholder="Adresa de email" id="adress"></li>
                        <li class="hotfix1"><input type="text" placeholder="Mesajul tau" id="message"></li>
                        <li class="hotfix1"><input type="submit" value="Trimite" id ="submitButton"></li>
                    </ul>
                    
                </ul>
                <div id="final" >
                    <p>Krisakis Travel agentie tur-operatoare inregistrata la Registrul Comertului sub numarul J24/1032/2012m CUI 30939314, Licenta de turism n: xxxxx/xx.xx.xxxx</p>
                    <p>Brevet de turism nr: XXXX/XX.XX.XXXX, Polita de asigurare nr: XXXXX/XX.XX.XXXX</p>
                </div>
            </div>
        </div>
    </div>

</body>
</html> 